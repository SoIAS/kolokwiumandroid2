package com.wdowiak.wdowiak_kolokwium;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Zadanie3 extends AppCompatActivity {

    int randomized_value = 0;
    int guess_count = 0;
    ArrayList<Integer> guessed_numbers;

    EditText et_input;
    TextView tv_guess_count, tv_already_guessed, tv_guess_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie3);

        et_input = findViewById(R.id.zadanie3_input);
        tv_guess_count = findViewById(R.id.zadanie3_tv_guess_count);
        tv_already_guessed = findViewById(R.id.zadanie3_tv_already_guessed);
        tv_guess_result = findViewById(R.id.zadanie3_tv_guess_result);

        findViewById(R.id.zadanie3_btn_gen_numebr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int min = -100, max = 100;

                Random random = new Random();
                randomized_value = random.nextInt(get_random_bound(min, max)) + min;
                guess_count = 0;
                guessed_numbers = new ArrayList<>();

                et_input.setText(null);
                tv_guess_count.setText(null);
                tv_already_guessed.setText(null);
                tv_guess_result.setText(null);

                Toast.makeText(Zadanie3.this, R.string.number_randomized, Toast.LENGTH_SHORT).show();

                final LinearLayout game_layout = findViewById(R.id.zadanie3_game_layout);
                game_layout.setVisibility(View.VISIBLE);

                Button this_button = (Button)view;
                this_button.setText(R.string.randomize_new_number);
            }
        });

        findViewById(R.id.zadanie3_btn_guess).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {
                    final String input_string = et_input.getText().toString();
                    if(input_string.isEmpty())
                    {
                        Toast.makeText(Zadanie3.this, R.string.have_to_enter_a_number, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    final int value = Integer.valueOf(input_string);

                    guessed_numbers.add(value);
                    ++guess_count;

                    if(value == randomized_value)
                    {
                        tv_guess_result.setText(R.string.correct_guess);
                        tv_guess_result.setTextColor(Color.GREEN);
                    }
                    else if(value > randomized_value)
                    {
                        tv_guess_result.setText(R.string.too_high);
                        tv_guess_result.setTextColor(Color.RED);
                    }
                    else
                    {
                        tv_guess_result.setText(R.string.too_low);
                        tv_guess_result.setTextColor(Color.RED);
                    }

                    et_input.setText(null);
                    update_tries();

                }
                catch(NumberFormatException e)
                {
                    e.printStackTrace();
                    Toast.makeText(Zadanie3.this, R.string.couldnt_parse, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private int get_random_bound(int min, int max)
    {
        return max - min + 1;
    }

    void update_tries()
    {
        tv_guess_count.setText(getResources().getString(R.string.guess_count) + String.valueOf(guess_count));
        tv_already_guessed.setText(getResources().getString(R.string.already_guessed) + guessed_numbers.toString());
    }
}
