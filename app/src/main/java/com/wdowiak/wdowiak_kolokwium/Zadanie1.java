package com.wdowiak.wdowiak_kolokwium;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;

public class Zadanie1 extends AppCompatActivity {

    TextView tv_result_x1, tv_result_x2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie1);

        tv_result_x1 = findViewById(R.id.zadanie1_result_x1);
        tv_result_x2 = findViewById(R.id.zadanie1_result_x2);

        findViewById(R.id.zadanie1_btn_calculate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText edit_a = findViewById(R.id.zadanie1_edit_a);
                final EditText edit_b = findViewById(R.id.zadanie1_edit_b);
                final EditText edit_c = findViewById(R.id.zadanie1_edit_c);

                final String string_a = edit_a.getText().toString();
                final String string_b = edit_b.getText().toString();
                final String string_c = edit_c.getText().toString();

                if(string_a.isEmpty() || string_b.isEmpty() || string_c.isEmpty())
                {
                    Toast.makeText(Zadanie1.this, R.string.have_to_enter_every_number, Toast.LENGTH_SHORT).show();

                    tv_result_x1.setText(null);
                    tv_result_x2.setText(null);
                    return;
                }

                try
                {
                    final double value_a = Double.valueOf(string_a);
                    final double value_b = Double.valueOf(string_b);
                    final double value_c = Double.valueOf(string_c);

                    final double delta = Math.pow(value_b, 2) - (4* value_a * value_c);

                    if(delta < 0)
                    {
                        tv_result_x1.setText(R.string.no_real);
                        tv_result_x2.setText(null);
                    }
                    else if(delta == 0)
                    {
                        final double x1_result = -value_b / (2 * value_a);

                        tv_result_x1.setText(getResources().getString(R.string.x1) + x1_result);
                        tv_result_x2.setText(null);                    }
                    else
                    {
                        final double x1_result = (-value_b - Math.sqrt(delta)) / (2 * value_a);
                        final double x2_result = (-value_b + Math.sqrt(delta)) / (2 * value_a);

                        tv_result_x1.setText(getResources().getString(R.string.x1)  + x1_result);
                        tv_result_x2.setText(getResources().getString(R.string.x2) + x2_result);
                    }
                }
                catch(NumberFormatException e)
                {
                    tv_result_x1.setText(R.string.couldnt_parse);
                    tv_result_x2.setText(null);

                    e.printStackTrace();
                }
            }
        });
    }

}
