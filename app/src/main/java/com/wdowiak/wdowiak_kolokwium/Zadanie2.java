package com.wdowiak.wdowiak_kolokwium;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Zadanie2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadanie2);

        findViewById(R.id.zadanie2_btn_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText input_view = findViewById(R.id.zadanie2_input);
                final String input_text = input_view.getText().toString();
                final TextView result_view = findViewById(R.id.zadanie2_result);

                if(input_text.isEmpty())
                {
                    Toast.makeText(Zadanie2.this, R.string.cannot_be_empty, Toast.LENGTH_SHORT).show();
                    result_view.setText(null);

                    return;
                }

                final boolean result = isPalindrome(input_text);
                result_view.setText(result ? R.string.is_palindrome : R.string.is_not_palindrome);
                result_view.setTextColor(result ? Color.GREEN : Color.RED);
            }
        });

        findViewById(R.id.zadanie2_btn_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText input_view = findViewById(R.id.zadanie2_input);
                input_view.setText(null);

                final TextView result_view = findViewById(R.id.zadanie2_result);
                result_view.setText(null);
            }
        });
    }

    private boolean isPalindrome(String str)
    {
        str = str.toLowerCase().replace(" ", "");
        final int length = str.length() / 2;
        for(int i = 0; i < length; ++i)
        {
            if(str.charAt(i) != str.charAt(str.length() - i - 1))
            {
                return false;
            }
        }

        // for all whitespaces
        /*for(int i = 0, j = str.length() - ; i < j;)
        {
            if(Character.isWhitespace(str.charAt(i)))
            {
                ++i;
                continue;
            }

            if(Character.isWhitespace(str.charAt(j)))
            {
                --j;
                continue;
            }

            if(str.charAt(i) != str.charAt(j))
            {
                return false;
            }

            ++i;
            --j;
        }*/

        return true;
    }
}
